import java.sql.*;
import java.util.Scanner;

public class tikoht {

	public static void main(String args[]) {
		
		String url = "jdbc:postgresql://dbstud2.sis.uta.fi:5432/vk431309";
		String kayttaja = "vk431309";
		String salasana = "Okosan";
		
		try {
			// Otetaan yhteys tietokantaan.
			Connection con = null;
			con = DriverManager.getConnection(url, kayttaja, salasana);	
			
			// Luodaan Scanner lukemaan käyttäjän syötteitä.
			Scanner lukija = new Scanner(System.in);
			Scanner intlukija = new Scanner(System.in);
			Scanner floatlukija = new Scanner(System.in);
			
			Statement stmt = con.createStatement();
			
			System.out.println("Tervetuloa laskutusohjelmaan. Kirjoita \"help\" saadaksesi tietoa ohjelman toiminnoista.");
			String syote = "";
			while (!syote.equals("exit")) {
				
				// Luetaan käyttäjän syöte.
				syote = lukija.nextLine();
				
				// Lisätään uusi asiakas tietokantaan.
				if (syote.equals("luoAsiakas")) {
					String asyote = "";
					
					System.out.println("Syötä asiakkaan nimi:");
					asyote = lukija.nextLine();
					
					// Asiakkaalle luodaan tunnusnumero taulukossa olevien asiakkaiden määrän perusteella.
					ResultSet asiakkaat = stmt.executeQuery("SELECT COUNT(*) FROM asiakas");
					
					if (asiakkaat.next()) {
						int asiakaslkm = asiakkaat.getInt(1);
						stmt.executeUpdate("INSERT INTO asiakas VALUES(" + (asiakaslkm + 1) + ", '" + asyote + "')");
						System.out.println("Luotu uusi asiakas " + asyote + ". Asiakkaan tunnusnumero on " + (asiakaslkm + 1));
					}
					asiakkaat.close();
				}
				
				// Lisätään uusi kohde tietokantaan.
				else if (syote.equals("luoKohde")) {
					String knimi = "";
					int atunnus = 0;
					int tt_id = 0;
					int urakka_id = 0;
					int lasku_id = 0;
					int kohde_id = 0;
					float urakkahinta = 0;
					int alennusprosentti = 0;
					String onurakka = "";
					
					// Luetaan käyttäjältä kohteen omistavan asiakkaan tunnusnumero ja kohteen osoite.
					System.out.println("Syötä asiakkaan tunnusnumero:");
					atunnus = intlukija.nextInt();
					System.out.println("Syötä kohteen osoite:");
					knimi = lukija.nextLine();
					
					// Luodaan kohteelle tunnusluku ja lisätään tiedot tietokantaan.
					ResultSet kohteet = stmt.executeQuery("SELECT COUNT(*) FROM kohde");
					if (kohteet.next()) {
						kohde_id = kohteet.getInt(1) + 1;
						stmt.executeUpdate("INSERT INTO kohde VALUES(" + kohde_id + ", " + atunnus + 
						", '" + knimi + "')");
					}
					kohteet.close();
					
					// Luodaan projektiin liittyvälle laskulle tunnusluku.
					ResultSet laskut = stmt.executeQuery("SELECT COUNT(*) FROM lasku");
					if (laskut.next()) {
						lasku_id = laskut.getInt(1) + 1;
						stmt.executeUpdate("INSERT INTO lasku VALUES(" + lasku_id + ')');
					}
					laskut.close();
					
					// Kysytään onko kyseessä urakka. Vastauksen perusteella yhdistetään tiedot oikeaan tauluun.
					System.out.println("Onko kyseessä urakka y/n");
					onurakka = lukija.nextLine();
					
					if (onurakka.equals("y")) {
						System.out.println("Kuinka paljon veloitat urakasta?");
						urakkahinta = floatlukija.nextFloat();
						ResultSet urakat = stmt.executeQuery("SELECT COUNT(*) FROM urakka");
						if (urakat.next()) {
							urakka_id = urakat.getInt(1) + 1;
							stmt.executeUpdate("INSERT INTO urakka (urakka_id, kohde_id, lasku_id, hinta, tyohinta)" +
							"VALUES(" + urakka_id + ", " + kohde_id + ", " + lasku_id + ", " +
							urakkahinta + ", " + urakkahinta + ')');
						}
						
						urakat.close();	
					}
					
					else {
						System.out.println("Haluatko antaa alennusta? (y/n)");
						String alennus = lukija.nextLine();
						if (alennus.equals("y")) {
							System.out.println("Kuinka montaprosenttia haluat antaa alennusta?");
							alennusprosentti = intlukija.nextInt();
						}
						ResultSet tyot = stmt.executeQuery("SELECT COUNT(*) FROM tuntityo");
						if (tyot.next()) {
							tt_id = tyot.getInt(1) + 1;
							stmt.executeUpdate("INSERT INTO tuntityo VALUES (" + tt_id + ", " +  kohde_id +
							", " + lasku_id + ", " + alennusprosentti + ", 0, 0, 0)");
						}
						tyot.close();
						
						ResultSet tyotyypit = stmt.executeQuery("SELECT COUNT(*) FROM tyo");
						if (tyotyypit.next()) {
							int t_id = tyotyypit.getInt(1) + 1;
							stmt.executeUpdate("INSERT INTO tyo VALUES(" + t_id + ", " + tt_id + ", 'tyo', 45, 0)");
							stmt.executeUpdate("INSERT INTO tyo VALUES(" + (t_id + 1) + ", " + 
							tt_id + ", 'suunnittelutyo', 55, 0)");
							stmt.executeUpdate("INSERT INTO tyo VALUES(" + (t_id + 2) + ", " + 
							tt_id + ", 'aputyo', 35, 0)");
						}
						tyotyypit.close();
					}
					System.out.println("Kohde luotu!");
					
				}
				
				// Lisätään tunteja kohteeseen ja lasketaan niiden perusteella kuluja.
				else if (syote.equals("lisaaTunteja")) {
					System.out.println("Valitse kohteen tunnusnumero, johon haluat lisätä työtunteja.");
					int kohde = intlukija.nextInt();
					System.out.println("Minkäläistä työtä teit? [tyo/suunnittelutyo/aputyo]");
					String tyotyyppi = lukija.nextLine();
					System.out.println("Kuinka monta tuntia teit töitä?");
					int tunnit = intlukija.nextInt();
					int vtunnit = 0;
					int tuntihinta = 0;
					float uusiraha = 0;
					float vanharaha = 0;
					float ktvahennys = 0;
					int vanhattunnit = 0;
					int tt_id = 0;
					
					ResultSet vanhatt = stmt.executeQuery("SELECT tt_id, tunnit, hinta FROM tuntityo " +
					"WHERE kohde_id = " + kohde);
					
					if (vanhatt.next()) {
						vanharaha = vanhatt.getFloat("hinta");
						vanhattunnit = vanhatt.getInt("tunnit");
						tt_id = vanhatt.getInt("tt_id");
					}
					vanhatt.close();
					
					// Käyttäjän syötteiden perusteella lisätään tunteja tyo-tauluun oikean attribuutin kohdalle
					// ja lasketaan tuntien ja tuntihintojen perusteella työn hinta.
					if (tyotyyppi.equals("tyo")) {
						ResultSet tuntikohde = stmt.executeQuery("SELECT tunnit FROM tyo WHERE " +
						"tt_id = " + tt_id + " AND tyyppi = 'tyo'");
						
						if (tuntikohde.next()) {
							vtunnit = tuntikohde.getInt(1);
						}
						tuntikohde.close();
						
						stmt.executeUpdate("UPDATE tyo SET tunnit = " + (vtunnit + tunnit) +
						" WHERE tt_id = " + tt_id + " AND tyyppi = 'tyo'");
						
						ResultSet palkat = stmt.executeQuery("SELECT tuntihinta FROM tyo WHERE tt_id = " + 
						tt_id + " AND tyyppi = 'tyo'");
						
						if (palkat.next()) {
							tuntihinta = palkat.getInt(1);
							uusiraha = tuntihinta * tunnit;
						}
						palkat.close();
						
						ktvahennys = (vanharaha + uusiraha) * (float)0.40 - (float)100;
						
						if (ktvahennys < 0) {
							ktvahennys = 0;
						}
						
						if (2250 < ktvahennys) {
							ktvahennys = 2250;
						}
						stmt.executeUpdate("UPDATE tuntityo SET tunnit = " + (vanhattunnit + tunnit) + 
						" WHERE tt_id = " + tt_id);
						
						stmt.executeUpdate("UPDATE tuntityo SET hinta = " + (vanharaha + uusiraha) +
						" WHERE tt_id = " + tt_id);
						
						stmt.executeUpdate("UPDATE tuntityo SET kt_vahennys = " + ktvahennys +
						" WHERE tt_id = " + tt_id);
					}
					
					if (tyotyyppi.equals("suunnittelutyo")) {
						ResultSet tuntikohde = stmt.executeQuery("SELECT tunnit FROM tyo WHERE " +
						"tt_id = " + tt_id + " AND tyyppi = 'suunnittelutyo'");
						
						if (tuntikohde.next()) {
							vtunnit = tuntikohde.getInt(1);
						}
						tuntikohde.close();
						
						stmt.executeUpdate("UPDATE tyo SET tunnit = " + (vtunnit + tunnit) +
						" WHERE tt_id = " + tt_id + " AND tyyppi = 'suunnittelutyo'");
						
						ResultSet palkat = stmt.executeQuery("SELECT tuntihinta FROM tyo WHERE tt_id = " + 
						tt_id + " AND tyyppi = 'suunnittelutyo'");
						
						if (palkat.next()) {
							tuntihinta = palkat.getInt(1);
							uusiraha = tuntihinta * tunnit;
						}
						palkat.close();
						
						ktvahennys = (vanharaha + uusiraha) * (float)0.40 - (float)100;
						
						if (ktvahennys < 0) {
							ktvahennys = 0;
						}
						
						if (2250 < ktvahennys) {
							ktvahennys = 2250;
						}
						stmt.executeUpdate("UPDATE tuntityo SET tunnit = " + (vanhattunnit + tunnit) + 
						" WHERE tt_id = " + tt_id);
						
						stmt.executeUpdate("UPDATE tuntityo SET hinta = " + (vanharaha + uusiraha) +
						" WHERE tt_id = " + tt_id);
						
						stmt.executeUpdate("UPDATE tuntityo SET kt_vahennys = " + ktvahennys +
						" WHERE tt_id = " + tt_id);
					}
					
					if (tyotyyppi.equals("aputyo")) {
						ResultSet tuntikohde = stmt.executeQuery("SELECT tunnit FROM tyo WHERE " +
						"tt_id = " + tt_id + " AND tyyppi = 'aputyo'");
						
						if (tuntikohde.next()) {
							vtunnit = tuntikohde.getInt(1);
						}
						tuntikohde.close();
						
						stmt.executeUpdate("UPDATE tyo SET tunnit = " + (vtunnit + tunnit) +
						" WHERE tt_id = " + tt_id + " AND tyyppi = 'aputyo'");
						
						ResultSet palkat = stmt.executeQuery("SELECT tuntihinta FROM tyo WHERE tt_id = " + 
						tt_id + " AND tyyppi = 'aputyo'");
						
						if (palkat.next()) {
							tuntihinta = palkat.getInt(1);
							uusiraha = tuntihinta * tunnit;
						}
						palkat.close();
						
						ktvahennys = (vanharaha + uusiraha) * (float)0.40 - (float)100;
						
						if (ktvahennys < 0) {
							ktvahennys = 0;
						}
						
						if (2250 < ktvahennys) {
							ktvahennys = 2250;
						}
						stmt.executeUpdate("UPDATE tuntityo SET tunnit = " + (vanhattunnit + tunnit) + 
						" WHERE tt_id = " + tt_id);
						
						stmt.executeUpdate("UPDATE tuntityo SET hinta = " + (vanharaha + uusiraha) +
						" WHERE tt_id = " + tt_id);
						
						stmt.executeUpdate("UPDATE tuntityo SET kt_vahennys = " + ktvahennys +
						" WHERE tt_id = " + tt_id);
					}
					System.out.println("Tunteja lisätty!");
				}
				
				// Lisää tarvikkeen haluttuun kohteeseen.
				else if (syote.equals("lisaaTarvike")) {
					System.out.println("Valitse kohteen tunnusnumero:");
					int kohde = intlukija.nextInt();
					
					// Käyttäjä syöttää tarvikkeen ominaisuudet.
					System.out.println("Mikä on lisättävän tarvikkeen nimi?");
					String tnimi = lukija.nextLine();
					System.out.println("Kuinka monta kappaletta tarviketta lisätään?");
					int tkpl = intlukija.nextInt();
					System.out.println("Mikä on tarvikkeen sisäänostohinta? (per kpl)");
					float sohinta = floatlukija.nextFloat();
					System.out.println("Monta metriä tarvikkeen pituus on? (valitse 0 jos pituudella ei ole väliä)");
					float pituus = floatlukija.nextFloat();
					float mhinta = sohinta * (float)1.20;
					int tt_id = -1;
					int urakka_id = -1;
					int tarvike_id = -1;
					float vhinta = 0;
					float tvhinta = 0;
					
					ResultSet tarvikkeet = stmt.executeQuery("SELECT COUNT(*) FROM tarvike");
					
					if (tarvikkeet.next()) {
						tarvike_id = tarvikkeet.getInt(1) + 1;
					}
					tarvikkeet.close();
					
					ResultSet tuntityot = stmt.executeQuery("SELECT tt_id FROM tuntityo " +
					"WHERE kohde_id = " + kohde);
					
					if (tuntityot.next()) {
						tt_id = tuntityot.getInt(1);
					}
					tuntityot.close();
					
					ResultSet urakat = stmt.executeQuery("SELECT urakka_id, hinta, tarvikehinta FROM urakka " +
					"WHERE kohde_id = " + kohde);
					
					if (urakat.next()) {
						urakka_id = urakat.getInt("urakka_id");
						vhinta = urakat.getFloat("hinta");
						tvhinta = urakat.getFloat("tarvikehinta");
					}
					urakat.close();
					
					// Jos kyseessä on tuntityö liitetään tiedot tuntityo-tauluun.
					if (-1 < tt_id) {
						
						stmt.executeUpdate("INSERT INTO tarvike (tarvike_id, tt_id, nimi, yksikko, sohinta, " +
						"pituus, mhinta) VALUES(" + tarvike_id + ", " + tt_id + ", '" + tnimi + "', " +
						tkpl + ", " + sohinta + ", " + pituus + ", " + mhinta + ")");
					}
					
					// Jos kyseessä on urakka tiedot liitetään urakka-tauluun.
					if (-1 < urakka_id) {
						stmt.executeUpdate("INSERT INTO tarvike (tarvike_id, urakka_id, nimi, yksikko, sohinta, " +
						"pituus, mhinta) VALUES(" + tarvike_id + ", " + urakka_id + ", '" + tnimi + "', " +
						tkpl + ", " + sohinta + ", " + pituus + ", " + mhinta + ")");
						
						stmt.executeUpdate("UPDATE urakka SET tarvikehinta = " + (tvhinta + (tkpl * mhinta)) + 
						" WHERE kohde_id = " + kohde);
						
						stmt.executeUpdate("UPDATE urakka SET hinta = " + (tkpl * mhinta + vhinta) + 
						" WHERE kohde_id = " + kohde);
					}
					
					System.out.println("Tarvike lisätty!");
				}
				
				// Lisää eräpäivämäärän kohteeseen liittyvälle laskulle.
				else if (syote.equals("lisaaEraPvm")) {
					System.out.println("Mikä on kohteen tunnusnumero?");
					int kohde = intlukija.nextInt();
					System.out.println("Syötä eräpäivämäärä:");
					String erapvm = lukija.nextLine();
					int lasku_id = -1;
					
					// Haetaan laskun tunnusluku.
					ResultSet ulaskut = stmt.executeQuery("SELECT lasku.lasku_id FROM lasku, urakka " +
					"WHERE urakka.kohde_id = " + kohde + " AND lasku.lasku_id = urakka.lasku_id");
					
					if (ulaskut.next()) {
						lasku_id = ulaskut.getInt(1);
					}
					ulaskut.close();
					
					ResultSet tlaskut = stmt.executeQuery("SELECT lasku.lasku_id FROM lasku, tuntityo " +
					"WHERE tuntityo.kohde_id = " + kohde + " AND lasku.lasku_id = tuntityo.lasku_id");
					
					if (tlaskut.next()) {
						lasku_id = tlaskut.getInt(1);
					}
					tlaskut.close();
					
					if (-1 < lasku_id) {
						stmt.executeUpdate("UPDATE lasku SET era_pvm = '" + erapvm + "' WHERE lasku_id = " + lasku_id);
					}
					
					System.out.println("Eräpäivämäärä lisätty!");
				}
				
				// Tulostaa kohteeseen liittyvän urakka- tai tuntityölaskun.
				else if (syote.equals("tulostaLasku")) {
					System.out.println("Mikä on kohteen tunnusnumero?");
					int kohde = intlukija.nextInt();
					
					String kohdenimi = "";
					String asiakas = "";
					int asiakas_id = 0;
					
					float uhinta = 0;
					float utarvikehinta = 0;
					float uthinta = 0;
					
					float thinta = 0;
					float sthinta = 0;
					float athinta = 0;
					int ttunnit = 0;
					int sttunnit = 0;
					int attunnit = 0;
					float kokohinta = 0;
					float ktvahennys = 0;
					int alennus = 0;
					
					int tt_id = -1;
					int urakka_id = -1;
					int lasku_id = -1;
					String erapvm = "";
					
					// Etsitään kohteen tiedot.
					ResultSet kohteet = stmt.executeQuery("SELECT osoite, asiakas_id FROM kohde WHERE kohde_id = " + 
					kohde);
					
					if (kohteet.next()) {
						kohdenimi = kohteet.getString("osoite");
						asiakas_id = kohteet.getInt("asiakas_id");
					}
					kohteet.close();
					
					// Etsitään kohteen omistavan asiakkaan nimi.
					ResultSet nimet = stmt.executeQuery("SELECT nimi FROM asiakas WHERE asiakas_id  = " + asiakas_id);
					
					if (nimet.next()) {
						asiakas = nimet.getString("nimi");
					}
					nimet.close();
					
					// Etsitään urakan tiedot.
					ResultSet urakat = stmt.executeQuery("SELECT urakka_id, hinta, tarvikehinta, tyohinta, lasku_id " +
					"FROM urakka WHERE kohde_id = " + kohde);
					
					if (urakat.next()) {
						urakka_id = urakat.getInt("urakka_id");
						uhinta = urakat.getFloat("hinta");
						uthinta = urakat.getFloat("tyohinta");
						utarvikehinta = urakat.getFloat("tarvikehinta");
						lasku_id = urakat.getInt("lasku_id");
					}
					urakat.close();
					
					// Etsitään tuntityön tiedot.
					ResultSet tuntityot = stmt.executeQuery("SELECT tt_id, lasku_id, kt_vahennys, " + 
					" alennus FROM tuntityo WHERE kohde_id = " + kohde);
					
					if (tuntityot.next()) {
						tt_id = tuntityot.getInt("tt_id");
						lasku_id = tuntityot.getInt("lasku_id");
						ktvahennys = tuntityot.getFloat("kt_vahennys");
						alennus = tuntityot.getInt("alennus");
					}
					tuntityot.close();
					
					// Lasketaan mahdollisesta alennuksesta johtuva alennuskerroin.
					float alekerroin = (float)1 - (float)alennus / (float)100.0;
					
					// Etsitään eräpäivämäärä.
					ResultSet laskut = stmt.executeQuery("SELECT era_pvm FROM lasku WHERE lasku_id = " + lasku_id);
					
					if (laskut.next()) {
						erapvm = laskut.getString(1);
					}
					
					// Tulostetaan tuntityölasku.
					if (-1 < tt_id) {
						System.out.println(asiakas + " tässä on lasku tuntityöstä suoritettuna kohteeseen " + 
						kohdenimi);
						System.out.println("");
						System.out.println("Saaja: Sähkötärsky OY");
						System.out.println("Tilinumero: FI01 2345 6789 98");
						
						// Lasketaan työtunnit ja niistä kertyvät maksut.
						ResultSet atkohde = stmt.executeQuery("SELECT tunnit, tuntihinta FROM tyo WHERE " +
						"tt_id = " + tt_id + " AND tyyppi = 'aputyo'");
					
						if (atkohde.next()) {
							attunnit = atkohde.getInt("tunnit");
							athinta = atkohde.getFloat("tuntihinta") * (float)attunnit * (float)alekerroin;
						}
						atkohde.close();
						
						ResultSet tkohde = stmt.executeQuery("SELECT tunnit, tuntihinta FROM tyo WHERE " +
						"tt_id = " + tt_id + " AND tyyppi = 'tyo'");
					
						if (tkohde.next()) {
							ttunnit = tkohde.getInt("tunnit");
							thinta = tkohde.getFloat("tuntihinta") * (float)ttunnit * (float)alekerroin;
						}
						tkohde.close();
						
						ResultSet stkohde = stmt.executeQuery("SELECT tunnit, tuntihinta FROM tyo WHERE " +
						"tt_id = " + tt_id + " AND tyyppi = 'suunnittelutyo'");
						
						if (stkohde.next()) {
							sttunnit = stkohde.getInt("tunnit");
							sthinta = stkohde.getFloat("tuntihinta") *(float)sttunnit * (float)alekerroin;
						}
						stkohde.close();
						
						kokohinta = sthinta + thinta + athinta;
						
						// Tulostetaan tarpeelliset tiedot laskuun.
						if (0 < alennus) {
							System.out.println("Alennus työstä: " + alennus + "%");
						}
						
						if (0 < ttunnit) {
							System.out.println("Työ (" + ttunnit + " tuntia): " + thinta + "€");
						}
						
						if (0 < sttunnit) {
							System.out.println("Suunnittelutyö (" + sttunnit + " tuntia): " + sthinta + "€");
						}
						
						if (0 < attunnit) {
							System.out.println("Aputyö (" + attunnit + " tuntia): " + athinta + "€");
						}
						
						ResultSet tarvikkeet = stmt.executeQuery("SELECT nimi, yksikko, pituus, mhinta FROM " +
						"tarvike WHERE tt_id = " + tt_id);
						
						// Etsitään tarvikkeiden tiedot ja tulostetaan ne.
						while(tarvikkeet.next()) {
							String tarvike = tarvikkeet.getString("nimi");
							int kpl = tarvikkeet.getInt("yksikko");
							float pituus = tarvikkeet.getFloat("pituus");
							float hinta = tarvikkeet.getFloat("mhinta");
							kokohinta = kokohinta + hinta * (float)kpl;
							if (pituus == 0) {
								System.out.println(tarvike + " (" + kpl + " kpl): " + hinta * (float)kpl + "€");
							}
							else {
								System.out.println(tarvike + " (" + kpl + " kpl) (" + pituus + "m): " + 
								hinta * (float)kpl + "€");
							}
						}
						
						System.out.println("Yhteensä: " + kokohinta + "€");
						System.out.println("Kotitalousvähennyskelpoisuus: " + ktvahennys + "€");
						System.out.println("Eräpäivämäärä: " + erapvm);
						
					}
					
					// Tulostetaan urakkalasku.
					if (-1 < urakka_id) {
						System.out.println(asiakas + " tässä on lasku urakasta suoritettuna kohteeseen " + 
						kohdenimi);
						System.out.println("");
						System.out.println("Saaja: Sähkötärsky OY");
						System.out.println("Tilinumero: FI01 2345 6789 98");
						System.out.println("Työ: " + uthinta + "€");
						System.out.println("Tarvikkeet: " + utarvikehinta + "€");
						System.out.println("Yhteensä: " + uhinta + "€");
						System.out.println("Eräpäivämäärä: " + erapvm);
					}
				}
				
				// Muodostetaan hinta-arvio tuntityöstä.
				else if (syote.equals("hintaArvio")) {
					System.out.println("Mikä on kohteen tunnusnumero?");
					int kohde = intlukija.nextInt();
					
					String kohdenimi = "";
					
					float thinta = 0;
					float sthinta = 0;
					float athinta = 0;
					int ttunnit = 0;
					int sttunnit = 0;
					int attunnit = 0;
					float kokohinta = 0;
					int alennus = 0;
					
					int tt_id = -1;
					
					ResultSet kohteet = stmt.executeQuery("SELECT osoite FROM kohde WHERE kohde_id = " + 
					kohde);
					
					if (kohteet.next()) {
						kohdenimi = kohteet.getString("osoite");
					}
					kohteet.close();
					
					// Etsitään tuntityön tunnusluku ja mahdollinen alennus.
					ResultSet tuntityot = stmt.executeQuery("SELECT tt_id, alennus FROM tuntityo " +
					"WHERE kohde_id = " + kohde);
					
					if (tuntityot.next()) {
						tt_id = tuntityot.getInt("tt_id");
						alennus = tuntityot.getInt("alennus");
					}
					tuntityot.close();
					
					// Lasketaan mahdollinen alennuskertoin.
					float alekerroin = (float)1 - (float)alennus / (float)100.0;
					
					if (-1 < tt_id) {
						System.out.println("Hinta arvio kohteesta " + kohdenimi);
						System.out.println("");
						if (0 < alennus) {
							System.out.println("Alennus työstä: " + alennus + "%");
						}
						
						// Lasketaan työtunnit ja niistä kertyvät maksut.
						ResultSet atkohde = stmt.executeQuery("SELECT tunnit, tuntihinta FROM tyo WHERE " +
						"tt_id = " + tt_id + " AND tyyppi = 'aputyo'");
					
						if (atkohde.next()) {
							attunnit = atkohde.getInt("tunnit");
							athinta = atkohde.getFloat("tuntihinta") * (float)attunnit * alekerroin;
						}
						atkohde.close();
						
						ResultSet tkohde = stmt.executeQuery("SELECT tunnit, tuntihinta FROM tyo WHERE " +
						"tt_id = " + tt_id + " AND tyyppi = 'tyo'");
					
						if (tkohde.next()) {
							ttunnit = tkohde.getInt("tunnit");
							thinta = tkohde.getFloat("tuntihinta") * (float)ttunnit * alekerroin;
						}
						tkohde.close();
						
						ResultSet stkohde = stmt.executeQuery("SELECT tunnit, tuntihinta FROM tyo WHERE " +
						"tt_id = " + tt_id + " AND tyyppi = 'suunnittelutyo'");
						
						if (stkohde.next()) {
							sttunnit = stkohde.getInt("tunnit");
							sthinta = stkohde.getFloat("tuntihinta") *(float)sttunnit * alekerroin;
						}
						stkohde.close();
						
						kokohinta = sthinta + thinta + athinta;
						
						if (0 < ttunnit) {
							System.out.println("Työ (" + ttunnit + " tuntia): " + thinta + "€");
						}
						
						if (0 < sttunnit) {
							System.out.println("Suunnittelutyö (" + sttunnit + " tuntia): " + sthinta + "€");
						}
						
						if (0 < attunnit) {
							System.out.println("Työ (" + attunnit + " tuntia): " + athinta + "€");
						}
						
						ResultSet tarvikkeet = stmt.executeQuery("SELECT nimi, yksikko, pituus, mhinta FROM " +
						"tarvike WHERE tt_id = " + tt_id);
						
						// Käydään kohteeseen liittyvät tarvikkeet läpi ja tulostetaan niistä kertyvät maksut.
						while(tarvikkeet.next()) {
							String tarvike = tarvikkeet.getString("nimi");
							int kpl = tarvikkeet.getInt("yksikko");
							float pituus = tarvikkeet.getFloat("pituus");
							float hinta = tarvikkeet.getFloat("mhinta");
							kokohinta = kokohinta + hinta * (float)kpl;
							if (pituus == 0) {
								System.out.println(tarvike + " (" + kpl + " kpl): " + hinta * (float)kpl + "€");
							}
							else {
								System.out.println(tarvike + " (" + kpl + " kpl) (" + pituus + "m): " + 
								hinta * (float)kpl + "€");
							}
						}
						
						System.out.println("Yhteensä: " + kokohinta + "€");
					}
				}
				
				// Tulostaa asiakkaiden tiedot.
				else if (syote.equals("asiakkaat")) {
					ResultSet asiakkaat = stmt.executeQuery("SELECT * FROM asiakas");
					while (asiakkaat.next()) {
						System.out.println("Asiakas: " + asiakkaat.getString("nimi"));
						System.out.println("Asiakas ID: " + asiakkaat.getInt("asiakas_id"));
						System.out.println("");
					}
				}
				
				// Tulostaa kohteiden tiedot.
				else if (syote.equals("kohteet")) {
					ResultSet kohteet = stmt.executeQuery("SELECT * FROM kohde");
					while (kohteet.next()) {
						System.out.println("Osoite: " + kohteet.getString("osoite"));
						System.out.println("Kohde ID: " + kohteet.getInt("kohde_id"));
						System.out.println("Asiakkaan ID: " + kohteet.getInt("asiakas_id"));
						System.out.println("");
					}
					
				}
				
				// Tulostaa tietoa ohjelmasta.
				else if (syote.equals("help")) {
					System.out.println("Lista komennoista:");
					System.out.println("luoAsiakas - luo uuden asiakkaan tietokantaan.");
					System.out.println("luoKohde - luo uuden kohteen tietokantaan.");
					System.out.println("lisaaTunteja - lisää tunteja tuntityö projektiin.");
					System.out.println("lisaaTarvike - lisää tarvikkeita projektiin.");
					System.out.println("lisaaEraPvm - lisää eräpäivämäärän projektiin liittyvälle laskulle.");
					System.out.println("tulostaLasku - tulostaa kohteeseen liittyvän laskun.");
					System.out.println("hintaArvio - laskee hinta-arvion käynnissä olevalle projektille.");
					System.out.println("asiakkaat - tulostaa listan asiakkaiden tiedoista.");
					System.out.println("kohteet - tulostaa listan kohteen tiedoista.");
					System.out.println("exit - sulkee ohjelman.");
				}
				else if (syote.equals("exit")) {
				}
				else {
					System.out.println("Tuntematon komento!");
				}
			}
			stmt.close();
		}
		catch (SQLException e) {
			System.out.println("Error!");
		}
		catch (Exception t) {
			System.out.println("Error!");
		}
	}
}
